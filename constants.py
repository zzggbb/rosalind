alphabet = 'ACGT'
index_of_base = {
    'A': 0,
    'C': 1,
    'G': 2,
    'T': 3
}
complement_of_base = {
    'A': 'T',
    'C': 'G',
    'G': 'C',
    'T': 'A'
}
amino_acid_of_codon = {
    'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V', 'UUC': 'F',
    'CUC': 'L', 'AUC': 'I', 'GUC': 'V', 'UUA': 'L', 'CUA': 'L',
    'AUA': 'I', 'GUA': 'V', 'UUG': 'L', 'CUG': 'L', 'AUG': 'M',
    'GUG': 'V', 'UCU': 'S', 'CCU': 'P', 'ACU': 'T', 'GCU': 'A',
    'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A', 'UCA': 'S',
    'CCA': 'P', 'ACA': 'T', 'GCA': 'A', 'UCG': 'S', 'CCG': 'P',
    'ACG': 'T', 'GCG': 'A', 'UAU': 'Y', 'CAU': 'H', 'AAU': 'N',
    'GAU': 'D', 'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D',
    'UAA': '$', 'CAA': 'Q', 'AAA': 'K', 'GAA': 'E', 'UAG': '$',
    'CAG': 'Q', 'AAG': 'K', 'GAG': 'E', 'UGU': 'C', 'CGU': 'R',
    'AGU': 'S', 'GGU': 'G', 'UGC': 'C', 'CGC': 'R', 'AGC': 'S',
    'GGC': 'G', 'UGA': '$', 'CGA': 'R', 'AGA': 'R', 'GGA': 'G',
    'UGG': 'W', 'CGG': 'R', 'AGG': 'R', 'GGG': 'G'
}
codons_of_amino_acid = {
    '$': ['UAA', 'UAG', 'UGA'],
    'A': ['GCU', 'GCC', 'GCA', 'GCG'],
    'C': ['UGU', 'UGC'],
    'D': ['GAU', 'GAC'],
    'E': ['GAA', 'GAG'],
    'F': ['UUU', 'UUC'],
    'G': ['GGU', 'GGC', 'GGA', 'GGG'],
    'H': ['CAU', 'CAC'],
    'I': ['AUU', 'AUC', 'AUA'],
    'K': ['AAA', 'AAG'],
    'L': ['CUU', 'CUC', 'UUA', 'CUA', 'UUG', 'CUG'],
    'M': ['AUG'],
    'N': ['AAU', 'AAC'],
    'P': ['CCU', 'CCC', 'CCA', 'CCG'],
    'Q': ['CAA', 'CAG'],
    'R': ['CGU', 'CGC', 'CGA', 'AGA', 'CGG', 'AGG'],
    'S': ['UCU', 'UCC', 'UCA', 'UCG', 'AGU', 'AGC'],
    'T': ['ACU', 'ACC', 'ACA', 'ACG'],
    'V': ['GUU', 'GUC', 'GUA', 'GUG'],
    'W': ['UGG'],
    'Y': ['UAU', 'UAC'],
}
codons_per_amino_acid = {
    '$': 3, 'A': 4, 'C': 2, 'D': 2, 'E': 2, 'F': 2, 'G': 4, 'H': 2, 'I': 3,
    'K': 2, 'L': 6, 'M': 1, 'N': 2, 'P': 4, 'Q': 2, 'R': 6, 'S': 6, 'T': 4,
    'V': 4, 'W': 1, 'Y': 2,
}
mass_of_amino_acid = {
    'A':  71.03711, 'C':  103.00919, 'D':  115.02694, 'E':  129.04259,
    'F':  147.06841, 'G':  57.02146, 'H':  137.05891, 'I':  113.08406,
    'K':  128.09496, 'L':  113.08406, 'M':  131.04049, 'N':  114.04293,
    'P':  97.05276, 'Q':  128.05858, 'R':  156.10111, 'S':  87.03203,
    'T':  101.04768, 'V':  99.06841, 'W':  186.07931, 'Y':  163.06333
}
