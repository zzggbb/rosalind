#!/bin/env python3

import sys
import numpy

import constants

"""
Given: A collection of DNA strings of equal length, in FASTA format.
Return: A consensus string and profile matrix for the collection.
"""
def consensus(dna_strings):
    m = len(dna_strings)
    n = len(dna_strings[0])

    profile = numpy.zeros((4, n), dtype=numpy.int8)
    consensus = ''

    for column in range(n):
        for row in range(m):
            base = dna_strings[row][column]
            index = constants.index_of_base[base]
            profile[index][column] += 1

        max_count = -1
        max_base = 'A'
        for i in range(4):
            count = profile[i][column]
            if count > max_count:
                max_count = count
                max_base = constants.alphabet[i]

        consensus += max_base

    return consensus, profile
