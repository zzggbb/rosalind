#!/bin/env python3

import sys
import util

"""
Given: A collection of 'k' DNA strings each in FASTA format.
Return: A longest common substring of the collection. If multiple solutions
        exist, you may return any single solution.

Notes on the algorithm:
- substrings are pulled from the shortest input string.
- it doesn't actually matter which input string serves as the substring source,
  because the substring we are searching for is common to all input strings.
- the shortest input string is used because the runtime will be minimized.
- substrings are pulled in order of descending length.
- this allows the search to end as soon as a substring is found to be
  common to  all input strings.
"""
def common_substr(seq_0, seqs, length):
    # look at all possible substrings of a given 'length'
    for start in range(len(seq_0) - length + 1):
        part = seq_0[start:start+length]

        # as soon as you find a sequence that doesn't contain this substring move on to the next substring
        # otherwise return this substring
        # note: indicates that there is at least one common substring of this length
        for seq in seqs:
            if part not in seq:
                break
        else:
            return part
    return ""

def longest_common_substr(seqs):
    # pop seq_0 to reduce number of sequences to search and eliminate need to re-extract each time
    seq_0 = seqs.pop(0)

    ## starting points for the bounds on the longest common substring used in the binary search ##
    left = 0
    # +1 added to allow entire sequence to be the longest common substring
    right = len(seq_0) + 1

    # repeat until left and right are adjacent
    #     use left + 1, because you've already eliminated the right most and you get an endless loop otherwise ...
    while left + 1 < right:
        # pick midpoint in lengths -- nb:  assumes integer math
        mid = (left + right) // 2

        # if any substring of length mid is common to all sequences
        #    look for substrings of this length or longer
        # otherwise look for substrings of this length or less
        if common_substr(seq_0, seqs, mid) != "":
            left = mid
        else:
            right = mid

    return common_substr(seq_0, seqs, left)

def lcsm(strings):
    # source is shortest input string
    source = min(strings, key=len)

    # no point in checking the source
    strings.remove(source)

    for candidate in util.descending_substrings(source):
        common = True
        for string in strings:
            if candidate not in string:
                common = False
                break
        if common:
            return candidate

lines = sys.stdin.read().splitlines()
strings = list(util.fasta(lines).values())
print(longest_common_substr(strings))
