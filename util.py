'''
module:      util.py
author:      Zane Bradley
description: dependency-free generic utility functions
'''

'''
decorator function which allows f to remember results it has
previously calculated
'''
def memoize(f):
    previous = {}
    def g(*args):
        if args in previous:
            return previous[args]
        previous[args] = f(*args)
        return previous[args]
    return g

'''
input: a list 'L'
output: generator yielding all permutations of elements in list 'L'
'''
def permute(elements):
    if len(elements) <= 1:
        yield elements
    else:
        for perm in permute(elements[1:]):
            for i in range(len(elements)):
                yield perm[:i] + elements[0:1] + perm[i:]

'''
input: a string 's' and a string 't' of equal length
output: the hamming distance between 's' and 't'
'''
def hamming(s, t):
    assert len(s) == len(t)
    diff = 0
    for i in range(len(s)):
        if s[i] != t[i]:
            diff += 1
    return diff

'''
input: two strings 's' and 't'
output: generator yielding all substring locations of 't' in 's'
'''
@memoize
def substring_indices(s, t):
    if not t:
        return
    for i in range(len(s)):
        j = 0
        while i+j < len(s) and j < len(t) and s[i+j] == t[j]:
            j += 1
        if j == len(t):
            yield i

'''
input: a string 's'
output: a generator yielding substrings of 's', in order of
descending length
'''
def descending_substrings(s):
    N = len(s)
    for i in range(0,N):
        for j in range(0,i+1):
            yield s[j:N-i+j]

'''
input: sequence 'seq', and integer 'size'
output: a generator yielding sub sequences of length 'size'
in the order they appear in the original sequence 'seq'.
'''
def chunks(seq, size):
  for i in range(0, len(seq) - size + 1, size):
    yield seq[i:i+size]

'''
input: integer 'n'
output: factorial of n
'''
def fact(n):
  product = 1
  for i in range(1, n + 1):
    product *= i
  return product

def overlap(a, b):
  pass
