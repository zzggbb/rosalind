import lib
import util
import uniprot
from motif import Motif

def test(name, actual, expected):
    if actual == expected:
        print("pass: {}".format(name))
    else:
        print("fail: {}".format(name))
        print("    actual: {}".format(actual))
        print("  expected: {}".format(expected))

rna_string = "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"
test(
    'lib: first_protein: regular input',
    ''.join(lib.first_protein(lib.codons(rna_string))),
    'MAMAPRTEINSTRING'
)
test(
    'lib: first_protein: no start codon',
    ''.join(lib.first_protein(lib.codons('UGU'))),
    ''
)
test(
    'lib: protein: regular input',
    lib.protein(lib.codons('AUGGCCAUGGCGUAA')),
    'MAMA'
)
test(
    'lib: protein: no start codon',
    lib.protein(lib.codons('CAUCAUUAA')),
    None
)
test(
    'lib: protein: no stop codon',
    lib.protein(lib.codons('AUGGCCAUGGCC')),
    None
)
test(
    'lib: fasta: sequencies on single lines',
    lib.fasta(['>id1', 'GATTACA', '>id2', 'TAGACCA']),
    {'id1': 'GATTACA', 'id2': 'TAGACCA'}
)
test(
    'lib: fasta: sequences split across multiple lines',
    lib.fasta(['>id1', 'GATTACA', 'ATAG', 'AGAC',
                '>id2', 'ATAC', 'GATAT', 'GACT']),
    {'id1': 'GATTACAATAGAGAC', 'id2': 'ATACGATATGACT'}
)
test(
    'lib: fasta: ignore blank lines',
    lib.fasta(['>id1', '', 'GAT', 'TAC', '', 'A',
                '>id2', 'ACAT', '', 'GAT']),
    {'id1': 'GATTACA', 'id2': 'ACATGAT'}
)
test(
    'util: permute: regular input',
    sorted(util.permute('ABC')),
    ['ABC', 'ACB', 'BAC', 'BCA', 'CAB', 'CBA']
)
test(
    'util: hamming: regular input',
    util.hamming('GATAC', 'GAGAG'),
    2
)
test(
    'util: substring_indices: regular input',
    list(util.substring_indices('ATATAGCTAT', 'AT')),
    [0, 2, 8]
)
test(
    'util: substring_indices: empty needle',
    list(util.substring_indices('AGACTACAGAC', '')),
    []
)
test(
    'util: substring_indices: empty haystack',
    list(util.substring_indices('', 'GAT')),
    []
)
test(
    'util: substring_indices: empty needle and haystack',
    list(util.substring_indices('', '')),
    []
)
test(
    'util: descending_substrings: regular input',
    list(util.descending_substrings('ATG')),
    ['ATG', 'AT', 'TG', 'A', 'T', 'G']
)
test(
    'util: descending_substrings: single character',
    list(util.descending_substrings('A')),
    ['A']
)
test(
    'util: descending_substrings: empty input',
    list(util.descending_substrings('')),
    []
)
test(
    'util: chunks: empty',
    list(util.chunks('', 2)),
    []
)
test(
    'util: chunks: too short',
    list(util.chunks('abcd', 5)),
    []
)
test(
    'util: chunks: evenly divisible',
    list(util.chunks('abcdef', 3)),
    ['abc', 'def']
)
test(
    'util: chunks: not evenly divisible',
    list(util.chunks('abcdef', 4)),
    ['abcd']
)
test(
    'motif: length: regular input',
    Motif("A{BCD}[EFG]HIJ").length(),
    6
)
test(
    'motif: length: empty input',
    Motif('').length(),
    0
)
test(
    'motif: groups: 1',
    Motif('A{BC}[DE]FGH').groups(),
    [('literal', 'A'), ('exclude', 'BC'), ('include', 'DE'), ('literal', 'FGH')]
)
test(
    'motif: groups: 2',
    Motif('{BCD}EFG[HIJ]').groups(),
    [('exclude', 'BCD'), ('literal', 'EFG'), ('include', 'HIJ')]
)
test(
    'motif: groups: 3',
    Motif('ABCDEFGH').groups(),
    [('literal', 'ABCDEFGH')]
)
test(
    'motif: match: true input',
    Motif('A[BCD]{F}').match('ABQ'),
    True
)
test(
    'motif: match: small input',
    Motif('ABC[DEF]{GHI}').match('ABCD'),
    False
)
test(
    'motif: match: big input',
    Motif('AB[CD]{EF}').match('ABCQQQQQQ'),
    True
)
test(
    'motif: match: false by literal',
    Motif('A[BCD]{F}').match('ZBQ'),
    False
)
test(
    'motif: match: false by include',
    Motif('A[BCD]{F}').match('AYQ'),
    False
)
test(
    'motif: match: false by exclude',
    Motif('A[BCD]{F}').match('ABF'),
    False
)
test(
    'motif: locations: 1',
    Motif('AB[CD]{EF}').locations('ABCQABDZ'),
    [0, 4]
)
test(
    'motif: locations: 2',
    Motif('AB[CD]EF{G}').locations('QQABCEFQQQQQABDEFHH'),
    [2, 12]
)
test(
    'uniprot: request: 1',
    uniprot.request('B5ZC00'),
    ('sp|B5ZC00|SYG_UREU1 Glycine--tRNA ligase OS=Ureaplasma urealyticum serovar 10 (strain ATCC 33699 / Western) OX=565575 GN=glyQS PE=3 SV=1', 'MKNKFKTQEELVNHLKTVGFVFANSEIYNGLANAWDYGPLGVLLKNNLKNLWWKEFVTKQKDVVGLDSAIILNPLVWKASGHLDNFSDPLIDCKNCKARYRADKLIESFDENIHIAENSSNEEFAKVLNDYEISCPTCKQFNWTEIRHFNLMFKTYQGVIEDAKNVVYLRPETAQGIFVNFKNVQRSMRLHLPFGIAQIGKSFRNEITPGNFIFRTREFEQMEIEFFLKEESAYDIFDKYLNQIENWLVSACGLSLNNLRKHEHPKEELSHYSKKTIDFEYNFLHGFSELYGIAYRTNYDLSVHMNLSKKDLTYFDEQTKEKYVPHVIEPSVGVERLLYAILTEATFIEKLENDDERILMDLKYDLAPYKIAVMPLVNKLKDKAEEIYGKILDLNISATFDNSGSIGKRYRRQDAIGTIYCLTIDFDSLDDQQDPSFTIRERNSMAQKRIKLSELPLYLNQKAHEDFQRQCQK')
)
test(
    'util: overlap: first is longer prefix',
    util.overlap('XABCD', 'BCDE'),
    [True, 3]
)
test(
    'util: overlap: first is longer suffix',
    util.overlap('FGHIJKLMN', 'DEFGHI'),
    [False, 4]
)
test(
    'util: overlap: first is shorter prefix',
    util.overlap('GHIJ', 'IJKLMN'),
    [True, 2]
)
test(
    'util: overlap: first is shorter suffix',
    util.overlap('XYZ', 'TUVWXY'),
    [False, 2]
)
test(
    'util: overlap: no overlap',
    util.overlap('ABC', 'XYZ'),
    None
)
