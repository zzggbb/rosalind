import util
import constants

'''
input: a DNA string 'dna'
output: dictionary counting the respective number of times that each
nucleobase occur in 'dna'
'''
def count_bases(dna):
    histogram = {}
    for base in dna:
        if base in histogram:
            histogram[base] += 1
        else:
            histogram[base] = 1
    return histogram

'''
input: a DNA string 'dna'
output: the transcribed RNA string of 'dna'
'''
def rna(dna):
    return dna.replace('T', 'U')

'''
input: a DNA string 'dna'
output: generator yielding the reverse complement of 's'
'''
def revc(dna):
    for base in dna[::-1]:
        yield constants.complement_of_base[base]

'''
input: a DNA string 'dna'
output: the GC content of 's'. The GC content of a DNA string is the total
percentage of 'G' or 'C' symbols in the string.
'''
def gc(dna):
    count = 0
    for base in dna:
        if base in 'GC':
            count += 1
    return count / len(dna)

'''
input: a fasta formatted string 'fasta'
output: the ID and GC content of the dna string in 's' with the highest
GC content
'''
def max_gc(fasta):
    max_value = -1
    for k, v in fasta.items():
        value = gc(v)
        if value > max_value:
            max_value = value
            max_id = k
    return max_id, max_value

'''
input: a DNA or RNA string 's'
output: the codons of 's'. If the length of 's' is not divisible by 3,
the incomplete codon is omitted from the output.
'''
def codons(s):
  return util.chunks(s, 3)

'''
input: a sequence of rna codons
output: a generator yielding the first protein string found in rna_codons
'''
def first_protein(rna_codons):
    started = False
    for codon in rna_codons:
        acid = constants.amino_acid_of_codon[codon]
        if acid == '$':
            break
        if acid == 'M':
            started = True
        if not started:
            continue
        yield acid

def protein(rna_codons):
    if constants.amino_acid_of_codon[next(rna_codons)] != 'M':
      return None

    out = 'M'
    for codon in rna_codons:
      acid = constants.amino_acid_of_codon[codon]
      if acid == '$':
        return out
      else:
        out += acid

    return None

'''
input: a dna string, and two integers 'i' and 'j'
output: a boolean indicating whether or not string[i...j] is equal
to the reverse complement
'''
def reverse_palindrome(string, i, j):
    while i <= j:
        if string[i] != constants.complement_of_base[string[j]]:
            return False
        i += 1
        j -= 1

    return True

'''
input: a fasta formatted array of strings
output: a dictionary representing 's'
'''
def fasta(lines):
    data = {}
    if lines[0][0] != '>':
        raise ValueError("input not fasta format")

    for line in lines:
        # ignore empty lines
        if not line:
            continue
        # create new entry
        if line[0] == '>':
            label = line[1:]
            data[label] = ""
        # append to existing entry
        else:
            data[label] += line
    return data
