class Motif(object):
  def __init__(self, motif):
    self.motif = motif

  '''
  output: length of motif, in characters
  '''
  def length(self):
    in_group = False
    length = 0
    for c in self.motif:
      if c in ['{', '[']:
        in_group = True
        length += 1

      elif c in ['}', ']']:
        in_group = False

      else:
        if not in_group:
          length += 1

    return length

  '''
  output: motif represented as an array of groups. the groups are represented
  by tuples. the first value in the tuple indicates the type of the group,
  either 'literal', 'exclude', or 'include'. The second value in the tuple is
  the actual data of the group. For example, the motif 'AB[CD]{EF}'
  represented as [('literal', 'AB'), ('include', 'CD'), ('exclude', 'EF')]
  '''
  def groups(self):
    groups = []
    group = ''
    state = 'initial'
    for c in self.motif:
      if c in '[{':
        if state == 'literal':
          groups.append(('literal', group))
          state = 'set'
          group = ''

      elif c == '}':
        groups.append(('exclude', group))
        group = ''
        state = 'initial'

      elif c == ']':
        groups.append(('include', group))
        group = ''
        state = 'initial'

      else:
        group += c
        if state == 'initial':
          state = 'literal'

    if state == 'literal':
      groups.append(('literal', group))

    return groups

  '''
  input: protein string
  output: if 'motif' matches beginning of 'protein'
  '''
  def match(self, protein):
    if len(protein) < self.length():
      return False

    j = 0
    for group_id, group in self.groups():
      if group_id == 'literal':
        if protein[j:j+len(group)] != group:
          return False
        j += len(group)

      if group_id == 'exclude':
        if protein[j] in group:
          return False
        j += 1

      if group_id == 'include':
        if protein[j] not in group:
          return False
        j += 1

    return True

  '''
  input: protein string
  output: an array of locations where the motif exists in 'protein'
  '''
  def locations(self, protein):
    output = []
    for offset in range(0, len(protein) - self.length() + 1):
      if self.match(protein[offset:]):
        output.append(offset)

    return output

