import requests

from lib import fasta

URL_TEMPLATE = 'https://www.uniprot.org/uniprot/{}.fasta'

def request(protein):
  response = requests.get(URL_TEMPLATE.format(protein)).text.splitlines()
  for k, v in fasta(response).items():
    return (k, v)
