#!/bin/env python3
import sys
import math
import numpy

import cons
import constants
import iprb
import lib
import motif
import uniprot
import util

SCRIPTS = '''
dna rna revc fib gc hamm iprb prot subs cons mrna perm prtm pper fibd
'''

def usage():
    print("usage: {} <script id>".format(sys.argv[0]))
    print("valid script ids:\n{}".format(SCRIPTS))
    sys.exit(1)

if len(sys.argv) == 1:
    usage()

arg = sys.argv[1]

if arg == 'dna':
    dna_string = sys.stdin.readline()[:-1]
    counts = lib.count_bases(dna_string)
    print(' '.join(str(counts[base]) for base in constants.alphabet))

elif arg == 'rna':
    print(lib.rna(sys.stdin.readline()[:-1]))

elif arg == 'revc':
    print(''.join(lib.revc(sys.stdin.readline()[:-1])))

elif arg == 'fib':
    n, k = numpy.loadtxt(sys.stdin)

    @util.memoize
    def fib(n, k):
        if n == 0: return 0
        if n == 1: return 1
        return k * breeding_pairs(n-2, k) + breeding_pairs(n-1, k)

    print(int(fib(n, k)))

elif arg == 'gc':
    data = lib.fasta(sys.stdin.read().splitlines())
    max_id, max_gc = lib.max_gc(data)
    print("{}\n{:.6f}".format(max_id, max_gc * 100))

elif arg == 'hamm':
    s = sys.stdin.readline()[:-1]
    t = sys.stdin.readline()[:-1]
    print(util.hamming(s, t))

elif arg == 'iprb':
    k, m, n = numpy.loadtxt(sys.stdin)
    print(iprb.p_dominant(k, m, n))

elif arg == 'prot':
    rna_string = sys.stdin.readline()[:-1]
    codons = lib.codons(rna_string)
    protein = lib.first_protein(codons)
    print(''.join(protein))

elif arg == 'subs':
    s = sys.stdin.readline()[:-1]
    t = sys.stdin.readline()[:-1]
    indices = util.substring_indices(s, t)
    print(' '.join(str(d) for d in indices))

elif arg == 'cons':
    fasta = lib.fasta(sys.stdin.read().splitlines())
    dna_strings = list(fasta.values())
    consensus, profile = cons.consensus(dna_strings)
    print(consensus)
    for i in range(4):
        base = constants.alphabet[i]
        row = ' '.join(str(count) for count in profile[i])
        print("{}: {}".format(base, row))

elif arg == 'mrna':
    n = 3
    protein_string = sys.stdin.readline()[:-1]
    for acid in protein_string:
        n *= constants.codons_per_amino_acid[acid]
    print(n % 10**6)

elif arg == 'perm':
    n = int(sys.stdin.read(1))
    set = list(range(1, n + 1))
    perms = list(util.permute(set))
    print(len(perms))
    for perm in perms:
        print(' '.join(str(i) for i in perm))

elif arg == 'prtm':
    protein_string = sys.stdin.readline()[:-1]
    total = 0
    for acid in protein_string:
        total += constants.mass_of_amino_acid[acid]
    print("{:.3f}".format(total))

elif arg == 'pper':
    n, k = numpy.loadtxt(sys.stdin)
    print(int(math.factorial(n)/math.factorial(n-k)) % 10**6)

elif arg == 'fibd':
  @util.memoize
  def fibd(n, m):
    if n <= 0: return 0
    if n == 1: return 1

    base = fibd(n-1,m) + fibd(n-2,m)
    if n <= m: return base
    if n == m + 1: return base - 1
    return base - fibd(n-m-1,m)

  n, m = numpy.loadtxt(sys.stdin)
  print(fibd(n, m))

elif arg == 'grph':
  fasta = lib.fasta(sys.stdin.read().splitlines())
  for ki, vi in fasta.items():
    for kj, vj in fasta.items():
      if ki == kj:
        continue
      if vi[-3:] == vj[:3]:
        print("{} {}".format(ki, kj))

elif arg == 'pmch':
  fasta = lib.fasta(sys.stdin.read().splitlines())
  for k, v in fasta.items():
    print(util.fact(v.count('A')) * util.fact(v.count('G')))

elif arg == 'mprt':
  n_glycosylation = motif.Motif('N{P}[ST]{P}')
  for uniprot_id in sys.stdin.read().splitlines():
    _, protein = uniprot.request(uniprot_id)
    locations = n_glycosylation.locations(protein)
    if locations:
      print(uniprot_id)
      print(' '.join(str(l + 1) for l in locations))

elif arg == 'tree':
  lines = sys.stdin.read().splitlines()
  v = int(lines[0]) # number of vertices
  e = len(lines) - 1 # number of edges
  ee = v - 1 # number of expected edges
  print(ee - e)

elif arg == 'inod':
  n = int(sys.stdin.readline()[:-1])
  print(n - 2)

elif arg == 'long':
  strings = lib.fasta(sys.stdin.read().splitlines()).values()

elif arg == 'orf':
  stdin = sys.stdin.read().splitlines()
  dna = list(lib.fasta(stdin).values())[0]
  rna = lib.rna(dna)
  revc = lib.rna(''.join(lib.revc(dna)))
  proteins = []

  for i in range(len(rna) - 2):
    protein = lib.protein(lib.codons(rna[i:]))
    if protein and protein not in proteins:
      proteins.append(protein)

  for i in range(len(revc) - 2):
    protein = lib.protein(lib.codons(revc[i:]))
    if protein and protein not in proteins:
      proteins.append(protein)

  print('\n'.join(proteins))

elif arg == 'prob':
  dna = sys.stdin.readline()[:-1]
  A = [float(x) for x in sys.stdin.readline()[:-1].split()]
  B = []

  for Ak in A:
    p = 1
    for c in dna:
      if c in 'GC':
        p *= Ak/2

      if c in 'TA':
        p *= (1-Ak)/2
    B.append(numpy.log10(p))

  output = ' '.join(['{:.3f}'.format(f) for f in B])
  print(output)

else:
    usage()
